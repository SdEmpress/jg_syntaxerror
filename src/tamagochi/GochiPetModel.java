/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jg_syntaxerror;

import com.sun.istack.internal.logging.Logger;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import sun.util.logging.PlatformLogger;

/**
 *
 * @author student
 */
public class GochiPetModel {
    private int HP;
    private int EP;
    private int SP;
    private int lvl;
    private String name;    

    public GochiPetModel(String petFile) {
        Path file = Paths.get("./"+petFile);
        this.HP = 50;
        this.EP = 50;
        this.SP = 50;
        this.lvl = 0;
        this.name = petFile.substring(5);
        
        initializeModel(file);
        
    }
    
    private boolean initializeModel(Path file){
        boolean flag = true;
        String line = null;
        
        Charset charset = Charset.forName("UTF-8");
        try( BufferedReader reader = Files.newBufferedReader(file, charset)){
            while ((line = reader.readLine()) != null){
                System.out.println(line);
            }
        }catch(IOException x){
            flag = false;
          
            if(x.getClass().getName().equals("java.nio.file.NoSuchFileException")){
                FileWriter testFile;
                try{
                    testFile = new FileWriter("sample/sample.txt", true);
                    testFile.write("HP: "+this.HP+"SP: "+this.SP+"EP: "+this.EP+"lvl: "+this.lvl+"Name: "+this.name);
                    testFile.close();
                }catch(IOException ex){
                    Logger.getLogger(GochiPetModel.class.getName()).log(Level.SEVERE, null, ex);
                }
                flag = createFile(file);
            }
        }
        System.out.println(line);
        return flag;
    }
    
    private boolean createFile(Path file){
        boolean flag = true;
        
        Charset charset = Charset.forName("UTF-8");
        String s = "HP: "+this.HP+"|SP: "+this.SP+"|EP: "+this.EP+"|lvl: "+this.lvl+"|Name: "+this.name;
        try(BufferedWriter writer = Files.newBufferedWriter(file, charset)){
            writer.write(s, 0, s.length());
        }catch(IOException x){
            System.err.format("Create IOException: %s%n", x);
            flag = false;
        }
        
        return flag;
    }
    public void setHP(int HP) {
        this.HP = HP;
    }

    public void setEP(int EP) {
        this.EP = EP;
    }

    public void setSP(int SP) {
        this.SP = SP;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHP() {
        return HP;
    }

    public int getEP() {
        return EP;
    }

    public int getSP() {
        return SP;
    }

    public int getLvl() {
        return lvl;
    }

    public String getName() {
        return name;
    }
    
}
